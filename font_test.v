module count(input clk, rst, output reg [3:0] out); 
  always @(posedge clk)
    if (rst) out <= 0;
    else out <= out + 1;
endmodule

module font_test();

reg clk, rst;
reg [7:0] ascii;
wire [3:0] row;
wire pixel;

count c1(clk, rst, row); 
font_rom font(clk, ascii, row, pixel);

initial begin
  clk = 0;
  rst = 0;
  ascii = 'h43;
end 

always #2 clk = ~clk; 

initial begin
  $display("\t\ttime,\tclk,\trst,\trow,\tpixel");
  //$monitor("%d,\t%b\t%b,\t%h,\t%b",$time,clk,rst,row,pixel);
  $monitor("\t%h,\t%b",row,pixel);
end

initial
  #69 $finish;

event reset_trigger;
event reset_trigger_done;

initial begin
  forever begin
    @(reset_trigger);
    @(negedge clk);
    rst = 1;
    @(negedge clk);
    rst = 0;
    -> reset_trigger_done;
  end
end

initial begin: RESET_TEST
  -> reset_trigger;
end

endmodule

module pll(input clk, output out_clk); 
  reg rst = 1'b1;
  reg bp = 1'b0;

  SB_PLL40_CORE #(
    .DIVR(4'b0000),
    .DIVF(7'b1000111),//1280
    //.DIVF(7'b0110100),//800
    //.DIVF(7'b0111000),//1920x1080
    .DIVQ(3'b011),//1280 
    //.DIVQ(3'b100),//800 
    //.DIVQ(3'b010),//1920x1080
    .FILTER_RANGE(3'b001)
  ) pll1 ( 
    .REFERENCECLK(clk),
    .PLLOUTGLOBAL(out_clk),
    .RESETB(rst),
    .BYPASS(bp)
  );
endmodule

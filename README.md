ice40 VGA terminal
====

Here is the beginnings of a VT100-compatible terminal implemented purely in Verilog. It parses enough VT-100 escapes to mostly display the output from `htop` running under Linux with the `TERM=vt100` variable set.

It outputs VGA video and runs on a Lattive HX8k evaluation board with simple (resistor + socket) expansion board made by my friend Thomas Flummer.

### The good:

* 1280x1024 resolution, 1-bit, monospaced font equalling 160x64 characters. 

* Plenty of space left: uses 205/960 logic blocks of the HX8K.

### The bad:

* No way to attach a keyboard yet.

* No support for reverse video, boldface fonts or UTF-8.

* Uses 30/32 BRAMs, 20 for the character buffer, 8 for the font ROM, and 1 for a buffer between the UART and parser, and 1 for a lookup table used to parse ASCII numbers to binary.

* It parses the terminal escape codes in Verilog, it's ugly as all hell.

* Probably buggy in other areas. 

* Needs clean-up to be generic over size of the output.

### Next steps:

* Add a way to talk to a keyboard - to begin with probably my custom mech over i2c.

* Integrate a memory controller and external and move the character buffer there to reclaim some BRAMs and add support for attributes/colour.

* Move the parsing to a soft-core processor.

* Found out how xterms tell curses programmes their size and implement that.

* Add support for multiple inputs/outputs - hardware tmux...

More info
----

This started out as my first not-completely-trivial Verilog project, originally done in the summer of 2016. I did most of it myself but got some pointers from knielsen and other good people at Labitat, Copenhagen's first hackerspace.

It targets Lattice's ICE40HX8k evaluation board with a simple VGA expansion board (resistors and a socket) made by flummer.

It does monochrome, monospaced text in 1280x1024. There's no textbuffer editing, you can only enter characters over UART. There was a big plan for a video terminal controller core, but it never made it past notes. As I found it, it could only display one line of text and there were some bugs.

In October of 2019 (having written no Verilog for three years), I dusted it off and added some more advanced handling of the character buffer, to handle multiple lines, linebreaks etc for a real buffer of 160x64 characters. This meant it was able to display this file when sent from the computer via the USB serial port on the HX8k evaluation board. 

In October and November, I have been adding support for some VT100 escape codes, namely cursor movements and clearing various parts of the screen. It is currently using about one fourth of the FPGA area; VT100 escapes are not really that simple to decode. Perhaps it would be smarter to integrate a soft core and write a command parser in software, but my goal for now was to get to know Verilog better. 

The escape code parsing is implemented with an iffy state machine (that's a pun - it's bad and has a lot of ifs), and there's a small, dedicatedd controller that handles moving the cursor and writing/clearing the buffer. As of November 8th 2019, this seems basically functional, although there are some bugs (at least backspace doesn't work properly). I will also want to add some semblance of UTF-8 support so I can write the three characters needed for my first language (ÆØÅ). Maybe this can be done by just translating those characters on input.

Input (keyboard support) will likely come by connecting via i2c to a new controller I think I will be make for my custom keyboard. PS/2 support would make sense, but too much sense to fit into a project that parses VT100 escapes in hardware.

Back in 2016, I also made a board with an SDRAM chip, for which knielsen made a controller based on example code from Lattice (which was completely broken, as far as I remember). He also made a board with an STM32F407 on it that plugged into the HX8k evaluation board and connected it to its memory bus. As far as I remember, this was only ever used to test the SDRAM controller. It worked, but was never put to anymore use. Once I have the basic set of VT100 commands ready, I think I will integrate this RAM board and controller and try to offload the character buffer itself, since 160x64 characters is over half of the BRAM blocks in the ICE40 HX8k. There isn't, at this resolution, enough BRAM to add things like colour or text decoration support, but I hope to do that once I have more RAM.

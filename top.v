`default_nettype none

module top(input clk, uart_rx, output uart_tx, output [2:0] red, green, blue, output hsync, vsync, output [7:0] leds);
wire clk, led;

wire vga_clk;
pll pll1(clk, vga_clk);

wire [11:0] x, y;
wire fb_enable, fb_reset;
wire hsync, vsync; 
wire [11:0] pixel_count, line_count;

vga_blank blank(vga_clk, pixel_count, line_count, hsync, vsync, fb_reset, fb_enable);
vga_adr_trans #(.FB_X_MAX(1280), .FB_Y_MAX(1024)) trans(vga_clk, pixel_count, line_count, fb_reset, fb_enable, x, y);

// this module maintains a buffer of character and a pointer into it
// and tells us which character is at which pixel x, y
wire buf_w;
wire [7:0] buf_in;
wire pixel;
nnvt term(clk, vga_clk, buf_w, x, y, buf_in, pixel);

// wire up the uart to the character buffer
wire rst;
wire uart_irq;
wire [7:0] uart_do;
wire [7:0] uart_di;
wire uart_wr;
assign rst = 0;
uart #(.CLOCK_DIVIDE(312)) uart0(.clk(clk),
  .rst(rst), 
  .rx(uart_rx), .tx(uart_tx),
  .received(uart_irq), 
  .rx_byte(uart_do),
  .tx_byte(uart_di),
  .transmit(uart_wr));
always @(posedge clk) begin
  if (uart_irq) begin 
    uart_di <= uart_do;
    uart_wr <= 1;
    leds <= uart_do;
    buf_in <= uart_do;
    buf_w <= 1; 
  end else begin
    leds <= leds;
    buf_w <= 0;
    uart_wr <= 0;
  end
end

assign red = ~fb_enable ? 0 : (pixel ? 'b11 : 0);
assign green = ~fb_enable ? 0 : (pixel ? 'b11 : 0);
assign blue = 0;

endmodule

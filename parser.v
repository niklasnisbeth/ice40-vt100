`default_nettype none

// read three ascii digits one at a time
// output 2.5 digit BCD for decoding with a lookup table
// 0-199 will fit in one 256x16 ice40 BRAM...

module digit_store(input w_clk, reset, input [7:0] i_digit, output reg [7:0] o_digit, output reg hundreds); 
  function [7:0] decode(input [7:0] i_char);
    integer ii;
    begin
    decode = 0;
    for (ii=0; ii<10; ii=ii+1)
      if (i_char == 'h30+ii) decode = ii;
    end
  endfunction

  initial begin
    hundreds <= 0;
    w_pointer <= 0;
  end

  reg [3:0] digits [0:2];
  reg [2:0] w_pointer;

  always @(*) begin
    case (w_pointer)
      0: o_digit = 0;
      1: o_digit = {4'b0, digits[0]};
      2: o_digit = {digits[0], digits[1]};
      3: begin
        o_digit = {digits[1], digits[2]};
        hundreds = 1;
      end
    endcase
  end

  always @(posedge w_clk) begin
    digits[w_pointer] <= decode(i_digit);
    w_pointer <= w_pointer + 1;
    if (reset) begin
      w_pointer <= 0;
      digits[0] <= 0;
      digits[1] <= 0;
      digits[2] <= 0;
      hundreds <= 0;
    end
  end
endmodule

module bcd2bin_lut(input clk, hundreds, input [7:0] i_code, output reg [7:0] o_decoded);
  reg [7:0] lut[0:511];
  initial $readmemh("bcd2bin.hex", lut);
  
  always @(posedge clk)
    o_decoded <= lut[{i_code, hundreds}];
endmodule

// accepts a stream of ascii characters 
// directs '0'-'9' to the digit store,
// triggers a command on ;, A, B, C, D, 

module parser(
  input clk, irq,
  input [7:0] i_char,
  output reg [7:0] param1, param2, cmd,
  output reg exec);

  function is_digit(input [7:0] i_char);
    is_digit = (i_char > 'h2f && i_char < 'h3a);
  endfunction

  reg dec_write, dec_reset;
  wire dec_hundreds;
  wire [7:0] dec_o;
  wire [7:0] dec_o_bin;
  digit_store ds1(dec_write, dec_reset, i_char, dec_o, dec_hundreds);
  bcd2bin_lut decode(clk, dec_hundreds, dec_o, dec_o_bin);

  // parser state
  // 0: slurping chars as normal
  // 1: have esc, want [ (also, reset decoder)
  // 2: have [, want cmd/semicolon 
  // 3: have semicolon -- we can only store two params, but we slurp many to
  //    drop modesetting commands
  // 6: drop next command
  // 7: drop next char
  reg [2:0] parser_state;
  
  // we want to delay exec flag one cycle
  reg do_exec;

  initial begin
    param1 = 0;
    param2 = 0;
    cmd = 0;
    dec_write = 0;
    dec_reset = 0;
    parser_state = 0;
  end

  always @(posedge clk) begin
    do_exec <= 0;
    exec <= do_exec;
    cmd <= (exec || do_exec)  ? cmd : 0;
    parser_state <= exec ? 0 : parser_state;
    dec_write <= 0;
    dec_reset <= 0;
    if (irq) begin
      if (parser_state == 0) begin
        if (i_char == 'h1b)
          parser_state <= 1;
        else begin
          param1 <= i_char;
          cmd <= 'b1111;
          do_exec <= 1;
        end
      end else if (parser_state == 1) begin
        dec_write <= 1;
        dec_reset <= 1;
        param1 <= 0;
        param2 <= 0;
        if (i_char == 'h5b)
          parser_state <= 2;
        // parenthesis expressions related to charsets, drop
        else if (i_char == 'h28 || i_char == 'h29)
          parser_state <= 7;
        else
          parser_state <= 0;
      end else if (parser_state == 7) begin
        // drop a character, revert to looking for new commands
        parser_state <= 0;
      end else begin // parsing commands
        if (is_digit(i_char))
          dec_write <= 1;
        else begin
          case(i_char)
            'h3b: begin
              // semicolon
              // we swallow all groups of ;-separated parameters here
              // we only really support 2 parameter commands, so we could
              // check if parser_state == 2, but we need to silently drop
              // longer chains and it's probably a mode command which we don't
              // undestand and therefore throw away, so it's fine.
              param1 <= dec_o_bin; // separater, store 
              parser_state <= 3;
              dec_write <= 1;
              dec_reset <= 1;
            end
            'h3f: begin
              // question mark
              // introduces mode setting commands that we ultimately don't care about
              parser_state <= 2;
            end
            'h41: begin
              // A, cursor up
              do_exec <= 1;
              param1 <= dec_o_bin;
              cmd <= 'b1001;
            end
            'h42: begin
              // B, cursor down
              do_exec <= 1;
              param1 <= dec_o_bin;
              cmd <= 'b1010;
            end
            'h43: begin
              // C, cursor right
              do_exec <= 1;
              param1 <= dec_o_bin;
              cmd <= 'b1011;
            end
            'h44: begin
              // D, cursor left
              do_exec <= 1;
              param1 <= dec_o_bin;
              cmd <= 'b1100;
            end
            'h48, 'h66: begin 
              // Hf, move abs
              do_exec <= 1;
              if (parser_state == 3) begin
                param2 <= dec_o_bin;
              end else begin
                param2 <= 0;
              end
              cmd <= 'b1000;
            end
            'h4a: begin
            // J
            case (dec_o_bin)
              0: begin
                // clear down
                do_exec <= 1;
                cmd <= 'b0011;
              end
              1: begin
                // clear up
                do_exec <= 1;
                cmd <= 'b0100;
              end
              2: begin
                // clear screen
                do_exec <= 1;
                cmd <= 'b0110;
              end
              default:
                parser_state <= 0;
            endcase
          end
          'h4b: begin
            // K
            case (dec_o_bin)
              0: begin 
              // clear_right
              cmd <= 'b0001;
              do_exec <= 1;
            end
            1: begin
              // clear left
              cmd <= 'b0010;
              do_exec <= 1;
            end
            2: begin
              // clear line
              cmd <= 'b0101;
              do_exec <= 1;
            end
            default:
              parser_state <= 0;
            endcase
          end
          default: // something's bad, abort, clear state
            parser_state <= 0;
        endcase 
      end
    end
  end
end
endmodule

`default_nettype none

module font_rom(input clk, input [2:0] x, input [3:0] y, input [7:0] ascii, output reg pixel); 
  reg [0:7] data [0:4095]; 
  initial $readmemh("font.list", data); 

  reg [2:0] ascii_x;
  reg [7:0] pixels;
  always @(posedge clk) begin 
    pixels <= data[{ascii, ~y}];
    pixel <= pixels[ascii_x];
    ascii_x <= ~(x-1);
  end
endmodule

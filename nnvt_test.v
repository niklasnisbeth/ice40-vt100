`timescale 1 ns/100 ps

module nnvt_test;
reg vga_clk;

wire [11:0] x, y;
wire fb_enable, fb_reset;

wire hsync, vsync; 
wire [11:0] pixel_count, line_count;

vga_blank blank(vga_clk, pixel_count, line_count, hsync, vsync, fb_reset, fb_enable);
vga_adr_trans #(.FB_X_MAX(1280), .FB_Y_MAX(1024)) trans(vga_clk, pixel_count, line_count, fb_reset, fb_enable, x, y);

wire clk;
wire buf_w;
wire [7:0] buf_in;
wire pixel;
nnvt term(clk, vga_clk, buf_w, x, y, buf_in, pixel);

initial begin
  $dumpfile("nnvt_test.vcd");
  $dumpvars(0, trans);
  $dumpvars(0, term);

  vga_clk <= 0;

  #25000000 $finish;
end

always #4 vga_clk <= !vga_clk;

endmodule

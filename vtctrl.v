`default_nettype none

/* Small FSM/processor that accepts commands and emits write operations
 to a character buffer.

We want to support these codes:
Esc[ValueA        Move cursor up n lines  CUU
Esc[ValueB        Move cursor down n lines  CUD
Esc[ValueC        Move cursor right n lines   CUF
Esc[ValueD        Move cursor left n lines  CUB
Esc[H             Move cursor to upper left corner  cursorhome
Esc[;H            Move cursor to upper left corner  cursorhome
Esc[Line;ColumnH  Move cursor to screen location v,h  CUP

Esc[f             Move cursor to upper left corner  hvhome
Esc[;f            Move cursor to upper left corner  hvhome
Esc[Line;Columnf  Move cursor to screen location v,h  CUP 

Esc[K             Clear line from cursor right  EL0
Esc[0K            Clear line from cursor right  EL0
Esc[1K            Clear line from cursor left   EL1
Esc[2K            Clear entire line   EL2 
Esc[J             Clear screen from cursor down   ED0
Esc[0J            Clear screen from cursor down   ED0
Esc[1J            Clear screen from cursor up   ED1
Esc[2J            Clear entire screen   ED2 

We assume values will be <255.

Commands are input as a nibble:

'b0001  CLEAR_RIGHT
'b0010  CLEAR_LEFT
'b0011  CLEAR_DOWN
'b0100  CLEAR_UP
'b0101  CLEAR_LINE
'b0110  CLEAR_SCREEN
'b1000  MOVE
'b1001  MOVE_REL_UP
'b1010  MOVE_REL_DOWN
'b1011  MOVE_REL_RIGHT
'b1100  MOVE_REL_LEFT
'b1111  WRITE_CHAR

MOVEs start by storing the cursor, then moving (iteratively).

MOVE_REL(DIR,N) store cursor and move N positions the specificied DIRection - 0 left, 1 up, 2 down, 3 right.

MOVE(X,Y) set cursor

If CLEAR is also specified, we clear iteratively, and then restore the cursor. We first decode to an absolute MOVE, which stores the cursor and always runs before the CLEAR.

CLEAR(X,Y) would mean clear, left-to-right, one character at a time, from the cursor position (after move!) until (X,Y), where X and Y are *relative to the starting point - without moving to the next line when the edge is reached, but moving to the far left when going down*. Ie., CLEAR(2,65) will clear to the very bottom of the buffer, regardless of where it starts.

*/

module vtctrl(
  input ctrl_clk,
  input [3:0] cmd,
  input exec,
  input [7:0] param1, param2,
  output ctrl_busy,
  output reg [7:0] mem_i, cur_x, cur_y,
  output reg mem_w,
  input ack
);

parameter BUF_SIZE_X = 160;
parameter BUF_SIZE_Y = 64;

// stored cursor
reg [7:0]  sto_x, sto_y;

// cmd handling signals
reg running;
reg [7:0] reg_param1, reg_param2; // for latching on cmd start
reg do_move_rel, do_clear, do_wr;

initial begin
  cur_x <= 0;
  cur_y <= 0;
  sto_x <= 0;
  sto_y <= 0;
  mem_w <= 0;
  mem_i <= 0;
  running <= 0;
  reg_param1 <= 0;
  reg_param2 <= 0;
  do_move_rel <= 0;
  do_clear <= 0;
end

assign ctrl_busy = running;

always @(posedge ctrl_clk) begin
  if (mem_w) begin
    if (ack)
      mem_w <= 0;
  end else if (running) begin
    if (do_move_rel) begin
      // Relative move
      // reg_param1 is the direction
      // reg_param2 is our counter
      case (reg_param1)
        0: begin // move left
          if (cur_x == 0 || reg_param2 == 0) begin
            running <= 0;
          end else begin
            cur_x <= cur_x - 1;
            reg_param2 <= reg_param2 - 1;
          end 
        end
        1: begin // move up
          if (cur_y == 0 || reg_param2 == 0)
            running <= 0;
          else begin
            cur_y <= cur_y - 1;
            reg_param2 <= reg_param2 - 1;
          end
        end
        2: begin // move down
          if (cur_y == BUF_SIZE_Y+1 || reg_param2 == 0)
            running <= 0;
          else begin
            cur_y <= cur_y + 1;
            reg_param2 <= reg_param2 - 1;
          end
        end
        3: begin // move right
          if (cur_x == BUF_SIZE_X+1 || reg_param2 == 0)
            running <= 0;
          else begin
            cur_x <= cur_x + 1;
            reg_param2 <= reg_param2 - 1;
          end
        end
      endcase
    end else if (do_wr) begin // write 1 character
      mem_w <= 1;
      running <= 0;
    end else if (do_clear) begin
      // continue clear
      if (mem_w == 0)
        mem_w <= 1;
      else begin
        if (reg_param1 == 0 || cur_x == BUF_SIZE_X) begin
          if (reg_param2 == 0 || cur_y == BUF_SIZE_Y) begin
            // all done! restore cursor somehow
            cur_x <= sto_x;
            cur_y <= sto_y;
            running <= 0;
          end else begin
            // next row
            reg_param2 <= reg_param2 - 1;
            reg_param1 <= BUF_SIZE_X;
            cur_x <= 0;
            cur_y <= cur_y + 1;
          end
        end else begin
          reg_param1 <= reg_param1 - 1;
          cur_x <= cur_x + 1;
        end
      end
    end
  end else if (exec) begin
    // latch all parameters do begin a new run
    mem_i <= 0;

    // store cursor
    sto_x <= cur_x;
    sto_y <= cur_y;

    running <= 1;
    do_move_rel <= 0;
    do_clear <= 0;
    do_wr <= 0;
    case (cmd)
      // 'b000: nothing
      'b1111: begin // write char
        case (param1)
          // backspace, delete
          'd8, 'd127: begin
            cur_x <= (cur_x == 0) ? 'd157 : cur_x - 1;
            cur_y <= (cur_x == 0) ? cur_y - 1 : cur_y; 
            mem_i <= 0;
            do_wr <= 1;
          end
          // lf
          'd10: begin
            cur_y <= cur_y + 1;
            cur_x <= 0;
            running <= 0;
          end
          // cr
          'd13: begin
            cur_x <= 0;
            running <= 0;
          end
          default: begin
            // null bytes and others we swallow
            if (param1 < 'h20) begin
              running <= 0;
            end else begin
              mem_i <= param1;
              cur_x <= (cur_x == 'd157) ? 0 : cur_x + 1;
              cur_y <= (cur_x == 'd157) ? cur_y + 1 : (cur_y == 'd63) ? 0 : cur_y;
              do_wr <= 1;
            end
          end
        endcase
      end
      'b1000: begin // absolute move, coordinates are 1-indexed
        cur_y <= param1 ? param1 - 1 : 0;
        cur_x <= param2 ? param2 - 1 : 0;
        running <= 0;
      end
      'b1001: begin // relative move up
        reg_param1 <= 1;
        reg_param2 <= param1;
        do_move_rel <= 1;
      end
      'b1010: begin // relative move down
        reg_param1 <= 2;
        reg_param2 <= param1;
        do_move_rel <= 1;
      end
      'b1011: begin // relative move right
        reg_param1 <= 3;
        reg_param2 <= param1;
        do_move_rel <= 1;
      end
      'b1100: begin // relative move left
        reg_param1 <= 0;
        reg_param2 <= param1;
        do_move_rel <= 1;
      end
      'b0001: begin // clear right
        do_clear <= 1;
        reg_param1 <= 65;
        reg_param2 <= 0;
      end
      'b0010: begin // clear left
        do_clear <= 1;
        cur_x <= 0;
        reg_param1 <= cur_x;
        reg_param2 <= 0;
      end
      'b0011: begin // clear down
        do_clear <= 1;
        reg_param1 <= 65;
        reg_param2 <= 160;
      end
      'b0100: begin // clear up
        do_clear <= 1;
        cur_y <= 0;
        cur_x <= 0;
        reg_param1 <= 65;
        reg_param2 <= cur_y;
      end
      'b0101: begin // clear line
        do_clear <= 1;
        cur_x <= 0;
        reg_param1 <= 160;
        reg_param2 <= 0;
      end
      'b0110: begin // clear screen
        do_clear <= 1;
        cur_y <= 0;
        cur_x <= 0;
        reg_param1 <= 160;
        reg_param2 <= 65;
      end
    endcase
  end
end

endmodule

`default_nettype none

module char_reader(input r_clk, ctrl_clk,
  input [7:0] cache_r_x, cur_y,
  output reg [7:0] o_char,
  // memory signals
  output reg mem_r,
  input mem_ack,
  output reg [7:0] mem_r_x, mem_r_y,
  input [7:0] mem_o);

initial begin
  o_char = 0;
  mem_r_x = 0;
  mem_r_y = 0;
  mem_r = 0;
end

// we keep two screen lines in cache and a variable to determine 
// which to read from
reg cur_cache_line; // 0 a = r, b = w; 1 a = w, b = r
reg [7:0] line_a[160];
reg [7:0] line_b[160];

// always @ read
// give them back the right character from the buffer
always @(posedge r_clk) begin
  if (cur_cache_line)
    o_char <= line_a[cache_r_x];
  else
    o_char <= line_b[cache_r_x];
end

// when cur_y changes
// start a read sequence to fill the other line with the next line from
// external memory

reg filling; // 0 full, 1 filling
reg [7:0] w_x;

initial begin
  filling = 1;
  cur_cache_line = 0;
  mem_r_x = 0;
end

always @(posedge ctrl_clk) begin
  if (cur_y != mem_r_y) begin
    mem_r_y <= cur_y;
    filling <= 1;
    cur_cache_line <= ~cur_cache_line;
  end
  if (filling) begin
    if (mem_ack) begin
      mem_r <= 0;
      mem_r_x <= mem_r_x + 1;
      if (cur_cache_line)
        line_b[mem_r_x] <= mem_o;
      else
        line_a[mem_r_x] <= mem_o; 
    end
    if (mem_r_x != 161) begin
      mem_r <= 1;
    end else if (mem_r_x == 161) begin 
      mem_r_x <= 0;
      filling <= 0;
    end
  end
end

endmodule

module ipbuf(input r_clk, w_clk, input [7:0] i_char, w_addr, r_addr, output reg [7:0] o_char);
  reg [7:0] chars[0:255];

  always @(posedge r_clk)
    o_char <= chars[r_addr];

  always @(posedge w_clk)
    chars[w_addr] <= i_char; 
endmodule

module input_fifo(
  input clk, i_irq, hold, 
  input [7:0] i_char, 
  output reg o_irq, 
  output wire [7:0] o_char);

reg [7:0] w_pointer;
reg [7:0] r_pointer;

ipbuf chars(clk, clk, i_char, w_pointer, r_pointer, o_char);

initial begin
  w_pointer = 0;
  r_pointer = 0;
  o_irq = 0;
end

always @(posedge clk) begin 
  if (i_irq) begin
    w_pointer <= w_pointer + 1;
  end 

  o_irq <= 0;
  if (!hold) begin
    if (!o_irq) begin
      if (w_pointer != r_pointer) begin
        r_pointer <= r_pointer + 1;
        o_irq <= 1;
      end
    end
  end 
end

endmodule

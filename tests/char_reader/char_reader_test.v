`default_nettype none

module test1();

reg r_clk, ctrl_clk;
wire [7:0] cur_x, cur_y;
wire [7:0] cache_o_char;

// memory
wire mem_r, mem_r_ack;
wire [7:0] mem_r_x, mem_r_y, mem_o;
// memory, unused
wire mem_w, mem_w_ack, mem_busy;
wire [7:0] mem_w_x, mem_w_y, mem_i;

// vga sim
reg [11:0] vga_x, vga_y;
initial begin
  vga_x = 0;
  vga_y = 0;
end

assign cur_x = vga_x[11:4];
assign cur_y = vga_y[11:4];

always @(posedge r_clk) begin
  vga_x <= vga_x + 1;
  if (vga_x == 1688) begin
    vga_x <= 0;
    vga_y <= (vga_y == 1065) ? 0 : vga_y + 1;
  end
end

char_mem mem1(ctrl_clk, 
  mem_r, mem_w,
  mem_i,
  mem_r_x, mem_r_y, mem_w_x, mem_w_y,
  mem_busy,
  mem_r_ack, mem_w_ack,
  mem_o);

char_reader cache1(r_clk, ctrl_clk,
  cur_x, cur_y,
  cache_o_char,
  mem_r,
  mem_r_ack,
  mem_r_x,
  mem_r_y,
  mem_o);

initial begin
  $dumpfile("cache-test.wave");
  $dumpvars();
end

always #2 r_clk = ~r_clk;
always #5 ctrl_clk = ~ctrl_clk;

initial begin
  r_clk = 0;
  ctrl_clk = 1;
end

initial begin
  @(negedge cache1.filling)
  @(negedge cache1.filling)
  @(negedge cache1.filling)
  @(negedge cache1.filling)

  #50 $finish;
end

endmodule

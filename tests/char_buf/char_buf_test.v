`default_nettype none

module test1();

reg clk;
reg r, w;
reg [7:0] i_char, i_col_r, i_row_r, i_col_w, i_row_w;

wire busy, r_ack, w_ack;
wire [7:0] o_char;

char_buf buf1(clk, 
  r, w, 
  i_char, 
  i_col_r, i_row_r, i_col_w, i_row_w, 
  busy, 
  r_ack, w_ack, o_char);

initial begin
  clk = 0;
  $dumpfile("buf-test.wave");
  $dumpvars();
end

// genvar i, j;
// for (i=0; i<2; i=i+1) begin
//   for (j=0; j<64; j=j+1) begin
//     initial $dumpvars(0,controller.buf1.buffer[i][j]);
//   end
// end

always #2 clk = ~clk;

initial begin
  r = 0;
  w = 0;
  i_char = 0;
  i_col_r = 0;
  i_row_r = 0;
  i_col_w = 0;
  i_row_w = 0;
end

initial begin
  w = 1;
  r = 0;
  i_char = 41;
  i_col_w = 5;
  i_row_w = 5;
  @(posedge w_ack) 
  w = 0;
  #10 r = 1;
  i_col_r = 5;
  i_row_r = 5;
  @(posedge r_ack) 
  r = 0;
  #11
  w = 1; 
  i_col_w = 3; i_row_w = 8;
  i_char = 66;
  #5
  r = 1;
  i_col_r = 3; i_row_r = 8;
  @(posedge w_ack)
  w = 0;
  @(posedge r_ack)
  r = 0;
  #100 $finish;
end

endmodule

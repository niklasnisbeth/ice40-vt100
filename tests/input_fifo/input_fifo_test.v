module input_buffer_test();

reg clk;
reg i_irq, hold;
reg [7:0] i_char;
wire o_irq;
wire [7:0] o_char;

initial begin
  clk = 0;
  i_irq = 0;
  hold = 0;
  i_char = 0;
end

input_fifo ipbf(clk, i_irq, hold, i_char, o_irq, o_char);

initial begin
  $dumpfile("input-buffer-test.wave");
  $dumpvars();
end

always
  #2 clk = ~clk;

task write_char();
  begin
    @(negedge clk)
      i_irq = 1;
    @(negedge clk)
      i_irq = 0;
  end
endtask

integer ii;

initial begin
  #17
  i_char = 'h40;
  write_char();
  #7 hold = 1;
  #4 i_char = 'h32;
  write_char();
  #4 i_char = 'h33;
  write_char();
  #4 i_char = 'h37;
  write_char();
  #4 i_char = 'h3a;
  write_char();
  #2 hold = 0;
  #8 hold = 1;
  #10 hold = 0;

  #5 hold = 1;
  #8 i_char = 'h32;
  write_char();
  #3 i_char = 'h33;
  write_char();
  #7 i_char = 'h37;
  write_char();
  #4 i_char = 'h3a;
  write_char();
  #3 hold = 0;

  #40 for (ii = 0; ii<300; ii=ii+1) begin
    #7 i_char = 'h37;
    write_char(); 
  end 
  #60 $finish;
end

endmodule

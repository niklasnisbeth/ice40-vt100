;; chicken scheme

(import srfi-28)

;; do something 256 times
(define (do256 proc)
  (letrec ((loop (lambda (c acc)
                  (if (= 255 c) 
                      (cons (proc c) acc)
                      (loop (+ c 1) (cons (proc c) acc))))))
    (loop 0 '())))

;; converts a two-digit packed BCD number into a hexadecimal number
(define (convert-number x)
  (let ((lsd (remainder x #x10))
        (msd (remainder (quotient x #x10) #x10)))
    (if (or (> lsd 9) (> msd 9))
        #xff
        (+ lsd (* 10 msd)))))

;; we alternately output a number 0-99 (or #xff) and 100-199
;; since the memory will output 16 bits, and we will use the "hundreds" flag
;; to select the upper/lower byte
(with-output-to-file 
  "bcd2bin.hex" 
  (lambda ()
    (do256 
      (lambda (x)
        (let ((digit (convert-number x)))
          (print (format "~x\n~x" digit (min #xff (+ 100 digit)))))))))

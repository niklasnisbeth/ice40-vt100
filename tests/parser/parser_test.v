module test1();

reg clk, reset, irq, busy;
reg [7:0] i_char;
wire [7:0] param1, param2, cmd;
wire exec;

initial begin
  clk = 0;
  irq = 0;
  i_char = 0;
  reset = 1;
end

parser sm1(clk, irq, i_char, param1, param2, cmd, exec);

integer i;
initial begin
  $dumpfile("bcd_test.wave");
  $dumpvars();
  for (i=0; i<3; i=i+1)
    $dumpvars(0,sm1.ds1.digits[i]);
end

always #2 clk = ~clk;

task write_char(); begin
  @(negedge clk)
    irq = 1;
  @(negedge clk)
    irq = 0;
  end
endtask

initial begin
  #21 reset = 0;
  #10 i_char = 'h33;
  write_char();
  #10 i_char = 'h32;
  write_char();
  #10 i_char = 'h32;
  write_char();
  #10 i_char = 'h3b;
  write_char();
  #10 i_char = 'h1b;
  write_char();
  #10 i_char = 'h5b;
  write_char();
  #10 i_char = 'h31;
  write_char();
  #10 i_char = 'h41;
  write_char();

  // H-code
  #20 i_char = 'h1b;
  write_char();
  #10 i_char = 'h5b;
  write_char();
  #10 i_char = 'h33;
  write_char();
  #10 i_char = 'h3b;
  write_char();
  #10 i_char = 'h35;
  write_char();
  #10 i_char = 'h48;
  write_char();

  // m-code, many ;s
  #20 i_char = 'h1b;
  write_char();
  #10 i_char = 'h5b;
  write_char();
  #10 i_char = 'h33;
  write_char();
  #10 i_char = 'h33;
  write_char();
  #10 i_char = 'h3b;
  write_char();
  #10 i_char = 'h33;
  write_char();
  #10 i_char = 'h33;
  write_char();
  #10 i_char = 'h3b;
  write_char();
  #10 i_char = 'h33;
  write_char();
  #10 i_char = 'h33;
  write_char();
  #10 i_char = 'h3b;
  write_char();
  #10 i_char = 'h35;
  write_char();
  #10 i_char = 'h6d;
  write_char();

  // ?-code
  #20 i_char = 'h1b;
  write_char();
  #10 i_char = 'h5b;
  write_char();
  #10 i_char = 'h3f;
  write_char();
  #10 i_char = 'h31;
  write_char();
  #10 i_char = 'h30;
  write_char();
  #10 i_char = 'h34;
  write_char();
  #10 i_char = 'h39;
  write_char();
  #10 i_char = 'h68;
  write_char();
  
  // charset codes with )(
  #30 i_char = 'h1b;
  write_char();
  #10 i_char = 'h28;
  write_char();
  #10 i_char = 'h41;
  write_char();
  #20 i_char = 'h1b;
  write_char();
  #10 i_char = 'h29;
  write_char();
  #10 i_char = 'h41;
  write_char();

  // ^[[1B^[[124D
  #20 i_char = 'h1b;
  write_char();
  #10 i_char = 'h5b;
  write_char();
  #10 i_char = 'h31;
  write_char();
  #10 i_char = 'h42;
  write_char();
  #10 i_char = 'h1b;
  write_char();
  #10 i_char = 'h5b;
  write_char();
  #10 i_char = 'h31;
  write_char();
  #10 i_char = 'h32;
  write_char();
  #10 i_char = 'h34;
  write_char();
  #10 i_char = 'h44;
  write_char();

  #100 $finish;
end

endmodule

module test1();

reg clk;
reg exec;
reg [3:0] cmd;
reg [7:0] param1, param2;
reg [7:0] x, y;

wire busy;
wire [7:0] cur_char;

vtbuf_ctrl controller(clk, clk,
  cmd, exec,
  param1, param2,
  busy,
  x, y,
  cur_char);

initial begin
  clk = 0;
  $dumpfile("cmd-test.wave");
  $dumpvars();
end

genvar i, j;
for (i=0; i<2; i=i+1) begin
  for (j=0; j<64; j=j+1) begin
    initial $dumpvars(0,controller.buf1.buffer[i][j]);
  end
end

always #2 clk = ~clk;

initial begin
  cmd = 0;
  param1 = 0;
  param2 = 0;
  exec = 0;
end

initial begin
  cmd = 'b1000;
  param1 = 8;
  param2 = 10;
  exec = 1;
  #4 exec = 0;
  cmd = 0;
  #10
  cmd = 'b1111;
  param1= 'h40;
  exec = 1;
  #3 exec = 0;
  #10
  cmd = 'b1111;
  param1= 'h40;
  exec = 1;
  #3 exec = 0;
  #10
  cmd = 'b1111;
  param1= 'h40;
  exec = 1;
  #3 exec = 0;
  #10
  cmd = 'b1111;
  param1= 'h40;
  exec = 1;
  #3 exec = 0;
  #40
  cmd = 'b0101;
  exec = 1;
  #5 exec = 0;
  #1300
  cmd = 'b1111;
  param1= 'h40;
  exec = 1;
  #3 exec = 0;
  @(negedge busy)
  #10 
  param1 = 'h7c;
  cmd = 'h0c;
  exec = 1;
  #4 exec = 0;
  @(negedge busy);
  #10 
  param1 = 'h7c;
  cmd = 'h0b;
  exec = 1;
  #4 exec = 0;
  @(negedge busy);
  #10 
  param1 = 'h7c;
  cmd = 'h0c;
  exec = 1;
  #4 exec = 0;
  @(negedge busy);
  #20 $finish;
end

endmodule

module input_buffer_test();

reg vga_clk;
reg clk;
reg [11:0] x, y;
reg i_irq;
reg [7:0] i_char;
wire pixel;

initial begin
  clk = 0;
  x = 0;
  y = 0;
  i_irq = 0;
  i_char = 0;
end

nnvt vt(clk, vga_clk, i_irq, x, y, i_char, pixel);

initial begin
  $dumpfile("nnvt-test.wave");
  $dumpvars();
end

always
  #2 clk = ~clk;

task write_char();
  begin
    @(negedge clk)
      i_irq = 1;
    @(negedge clk)
      i_irq = 0;
  end
endtask

initial begin
  #17
  i_char = 'h40;
  write_char();
  #8 i_char = 'h32;
  write_char();
  #8 i_char = 'h33;
  write_char();
  #8 i_char = 'h37;
  write_char();
  #8 i_char = 'h3a;
  write_char();

  // an m sequence we don't care for
  #8 i_char = 'h1b;
  write_char();
  #4 i_char = 'h5b;
  write_char();
  #4 i_char = 'h32;
  write_char();
  #4 i_char = 'h33;
  write_char();
  #4 i_char = 'h6d;
  write_char();
  
  // a move seq we do
  #14 i_char = 'h1b;
  write_char();
  #5 i_char = 'h5b;
  write_char();
  #5 i_char = 'h33;
  write_char();
  #5 i_char = 'h34;
  write_char();
  #5 i_char = 'h3b;
  write_char();
  #5 i_char = 'h33;
  write_char();
  #5 i_char = 'h36;
  write_char();
  #5 i_char = 'h48;
  write_char();

  // an ( sequence we don't care for
  #17 i_char = 'h1b;
  write_char();
  #4 i_char = 'h28;
  write_char();
  #4 i_char = 'h32;
  write_char();
  #5 i_char = 'h33;
  write_char();
  #6 i_char = 'h6d;
  write_char();

  // normal characters
  #29 i_char = 'h42;
  write_char();
  #4 i_char = 'h43;
  write_char();
  #5 i_char = 'ha;
  write_char();

  // clear screen
  #18 i_char = 'h1b;
  write_char();
  #4 i_char = 'h5b;
  write_char();
  #4 i_char = 'h32;
  write_char();
  #4 i_char = 'h4a;
  write_char();

  // normal characters
  #14 i_char = 'h42;
  write_char();
  #8 i_char = 'h43;
  write_char();

  #120000 $finish;
end

endmodule

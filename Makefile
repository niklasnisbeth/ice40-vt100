ARACHNE_OPTS=-d 8k -P ct256 -r

vga.bin: top.v nnvt.v input_fifo.v pll.v parser.v char_mem.v char_reader.v vtctrl.v vga.v font.v vga.pcf ip/osdvu/uart.v
	yosys -s build.yosys -p "write_blif -gates -attr -param vga.blif"
	arachne-pnr $(ARACHNE_OPTS) vga.blif -p vga.pcf > vga.txt
	icepack vga.txt vga.bin

clean:
	rm -f vga.blif vga.txt vga.bin

flash: vga.bin
	iceprog vga.bin

test: nnvt_test.v
	iverilog -o test parser.v cmd.v vga.v font.v char_buf.v nnvt_test.v
	./test

gtkwave: test
	gtkwave nnvt_test.vcd

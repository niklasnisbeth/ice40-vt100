`default_nettype none

/* TODO replace with external memory
 * so we need to output when the memory is busy and handle that throughout
 * step 1: add req/busy/ack signals to the controller
 * step 2: add fake latency to the BRAM
 */
module char_mem (input clk, 
  input r, w, 
  input [7:0] i_char, 
  input [7:0] i_col_r, i_row_r, i_col_w, i_row_w,
  output reg busy, 
  output reg r_ack, w_ack, 
  output reg [7:0] o_char);

  reg [7:0] buffer[0:157][0:63];

  reg [0:2] delay;

  reg cmd; // 0: read 1: write

  initial begin
    delay <= 0;
    cmd <= 0;
    o_char <= 0;
    busy <= 0;
    r_ack <= 0;
    w_ack <= 0;
  end

  always @(posedge clk) begin
    w_ack <= 0;
    r_ack <= 0;
    if (busy) begin
      delay <= delay + 1;
      if (delay == 'b111) begin
        if (cmd)
          w_ack <= 1;
        else
          r_ack <= 1;
        busy <= 0;
      end else if (delay == 'b100) begin
        if (cmd)
          buffer[i_col_w][i_row_w] <= i_char;
        else
          o_char <= buffer[i_col_r][i_row_r];
      end
    end else begin
      if (w) begin
        busy <= 1;
        cmd <= 1;
      end else if (r) begin
        busy <= 1;
        cmd <= 0;
      end
    end
  end
endmodule


`default_nettype none

module nnvt(input ctrl_clk, r_clk, w, input [11:0] x, y, input [7:0] char_in, output wire pixel);
  wire [7:0] r_row;
  assign r_row = y[11:4];
  wire [7:0] r_col;
  assign r_col = x[11:3];

  wire [7:0] char_out;
  wire [7:0] ascii_out;
  assign ascii_out = (r_col < 'd160 && r_row < 'd64) ? char_out : 0;

  font_rom rom(r_clk, x[2:0], y[3:0], ascii_out, pixel);

  wire ctrl_busy, ctrl_exec;
  wire [3:0] ctrl_cmd;
  wire [7:0] ctrl_param1, ctrl_param2;

  wire fifo_ready;
  wire [7:0] fifo_out;

  wire mem_r, mem_w, mem_busy, mem_ack_r, mem_ack_w;
  wire [7:0] mem_char_i, mem_char_o;
  wire [7:0] mem_col_r, mem_row_r, mem_col_w, mem_row_w;

  // we store characters from the uart in a fifo if the parser/ctrl step is busy
  input_fifo i_fifo(ctrl_clk, w, ctrl_busy, char_in, fifo_ready, fifo_out);

  // the parser understands VT100 escapes and turns them into commands for the
  // controller
  parser p1(ctrl_clk, fifo_ready, fifo_out,
    ctrl_param1, ctrl_param2, ctrl_cmd, ctrl_exec);

  // the controller sequences writes to the character memory
  vtctrl c1(ctrl_clk,
    ctrl_cmd, ctrl_exec, 
    ctrl_param1, ctrl_param2,
    ctrl_busy,
    mem_char_i, mem_col_w, mem_row_w, mem_w,
    mem_ack_w);

  // the reader sequences reads to the character memory and maintains
  // a one-line cache of characters that is used for accessing the font
  char_reader reader(r_clk, ctrl_clk, 
    r_col, r_row, 
    char_out, 
    mem_r,
    mem_ack_r,
    mem_col_r,
    mem_row_r,
    mem_char_o);

  // the character memory presents the memory as an array of rows by columns
  // with control signals
  char_mem mem(ctrl_clk,
    mem_r, mem_w,
    mem_char_i,
    mem_col_r, mem_row_r, mem_col_w, mem_col_w,
    mem_busy,
    mem_ack_r, mem_ack_w,
    mem_char_o);

endmodule
